<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use DB;

class WellcomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         //show for home blade
        $published_blog = DB::table('tbl_blog')
                ->join('tbl_category','tbl_blog.category_id', '=', 'tbl_category.category_id')
                ->where('tbl_blog.publication_status',1)
                ->select('tbl_blog.*', 'tbl_category.category_name')
                ->get();
        
        $home_page = view('pages.home')->with('published_blog',$published_blog);
        $categories = view('pages.categories');
        $recent_post = view('pages.recent_post');
        return view('master')
                ->with('content', $home_page)
                ->with('categories', $categories)
                ->with('recent', $recent_post);
        //return 'Ronnie';
    }

    public function profile()
    {
        $portfolio_page = view('pages.portfolio');
        return view('master')->with('content', $portfolio_page);
        //return 'Ronnie';
    }
    
    public function service()
    {
        $service_page = view('pages.service');
        return view('master')->with('content', $service_page);
        //return 'Ronnie';
    }
    
    public function contact()
    {
        $contact_page = view('pages.contact');
        return view('master')->with('content', $contact_page);
        //return 'Ronnie';
    }
    
    public function blog_details($blog_id){
        $blog_info_details = DB::table('tbl_blog')
                    ->join('tbl_category', 'tbl_blog.category_id', '=', 'tbl_category.category_id')
                ->where('tbl_blog.blog_id', $blog_id)
                ->first();
        
        $blog_details = view('pages.blog_details')
                    ->with('blog_info_details',$blog_info_details);
        
        return view('master')->with('content',$blog_details);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
