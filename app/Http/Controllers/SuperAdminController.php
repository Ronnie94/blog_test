<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use DB;

class SuperAdminController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $admin_id = Session::get('admin_id');

        if ($admin_id == null) {
            return redirect::to('/admin-panel')->send();
        }
        $dashboard_content = view('admin.pages.dashboard_content');
        return view('admin.admin_master')
                        ->with('dashboard_content', $dashboard_content);
    }

    public function logout() {
        Session::put('admin_name', null);
        Session::put('admin_id', null);

        Session::put('message', 'You are Successfully Logout');
        return redirect::to('admin-panel');
    }

    public function add_category() {
        //return 'add category';
        $add_category_content = view('admin.pages.add_category');
        return view('admin.admin_master')
                        ->with('dashboard_content', $add_category_content);
    }

    public function save_category(Request $request) {
        $data = array();
        $data['category_name'] = $request->category_name;
        $data['category_desc'] = $request->category_desc;
        $data['publication_status'] = $request->publication_status;
      

        DB::table('tbl_category')->insert($data);

        Session::put('message', 'Save Category Information Successfully');
        return redirect::to('/add-category');
    }

    public function manage_category() {
        $category_info = DB::table('tbl_category')->get();
        $manage_category_content = view('admin.pages.manage_category')
                ->with('all_category_info', $category_info);

        return view('admin.admin_master')
                        ->with('dashboard_content', $manage_category_content);
    }

    public function unpublished_category($category_id) {
        //echo category_id;
        DB::table('tbl_category')
                ->where('category_id', $category_id)
                ->update(['publication_status' => 0]);

        return redirect::to('/manage-category');
    }

    public function published_category($category_id) {
        //echo category_id;
        DB::table('tbl_category')
                ->where('category_id', $category_id)
                ->update(['publication_status' => 1]);

        return redirect::to('/manage-category');
    }

    public function delete_category($category_id) {
        DB::table('tbl_category')
                ->where('category_id', $category_id)
                ->delete();

        return redirect::to('/manage-category');
    }

    public function edit_category($category_id) {
        //return 'xyz';
        $category_info = DB::table('tbl_category')
                ->where('category_id', $category_id)
                ->first();

        $edit_category_content = view('admin.pages.edit_category')
                ->with('category_info', $category_info);
        return view('admin.admin_master')
                        ->with('dashboard_content', $edit_category_content);
    }

    public function update_category(Request $request) {
        $data = array();
        $category_id = $request->category_id;
        $data['category_name'] = $request->category_name;
        $data['category_desc'] = $request->category_desc;
        $data['publication_status'] = $request->publication_status;

        DB::table('tbl_category')
                ->where('category_id', $category_id)
                ->update($data);

        return redirect::to('/manage-category');
    }

    public function add_blog() {
        $category_info = DB::table('tbl_category')
                ->where('publication_status', 1)
                ->get();

        $add_blog = view('admin.pages.add_blog')
                ->with('category_info', $category_info);

        return view('admin.admin_master')
                        ->with('dashboard_content', $add_blog);
    }

    public function save_blog(request $request) {
        $data = array();
        $data['blog_title'] = $request->blog_title;
        $data['category_id'] = $request->category_id;
        $data['blog_short_desc'] = $request->blog_short_desc;
        $data['blog_long_desc'] = $request->blog_long_desc;
        $data['publication_status'] = $request->publication_status;

        //for image Upload
        $image = $request->file('blog_image');


        if ($image) {
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'blog_image/';
            $image_url = $upload_path . $image_full_name;
            $success = $image->move($upload_path, $image_full_name);
            if ($success) {
                $data['blog_image'] = $image_url;
                DB::table('tbl_blog')->insert($data);
                Session::put('message', 'Save blog information Successfully');
                return redirect::to('/add-blog');
            }
        } else {
            DB::table('tbl_blog')->insert($data);
            Session::put('message', 'Save blog information Successfully');
            return redirect::to('/add-blog');
        }
    }

    public function manage_blog() {
        $blog_info = DB::table('tbl_blog')->get();
        $manage_blog_content = view('admin.pages.manage_blog')
                ->with('blog_info', $blog_info);

        return view('admin.admin_master')
                        ->with('dashboard_content', $manage_blog_content);
    }

    public function unpublished_blog($s_blog) {
        //return 'xyz';
        DB::table('tbl_blog')
                ->where('blog_id', $s_blog)
                ->update(['publication_status' => 0]);

        return redirect::to('/manage-blog');
    }

    public function published_blog($s_blog) {
        DB::table('tbl_blog')
                ->where('blog_id', $s_blog)
                ->update(['publication_status' => 1]);
        return redirect::to('/manage-blog');
    }

    public function delete_blog($s_blog) {
        //return 'sd';
        DB::table('tbl_blog')
                ->where('blog_id', $s_blog)
                ->delete();
        return redirect::to('/manage-blog');
    }

    public function edit_blog($s_blog) {
        //return 'xyz';
        $blog_info = DB::table('tbl_blog')
                ->leftJoin('tbl_category', 'tbl_blog.category_id', '=', 'tbl_category.category_id')
                ->where('blog_id', $s_blog)
//                ->select('tbl_blog.*', 'tbl_category.category_name')
                ->first();

        $category_info = DB::table('tbl_category')
                ->where('publication_status', 1)
                ->get();

        $edit_blog_content = view('admin.pages.edit_blog')
                ->with('blog_info', $blog_info)
                ->with('cat_list', $category_info);
        return view('admin.admin_master')
                        ->with('dashboard_content', $edit_blog_content);
    }

    public function update_blog(Request $request) {
        $data = array();
        $blog_id = $request->blog_id;
        $data['blog_title'] = $request->blog_title;
        $data['category_id'] = $request->category_id;
        $data['blog_short_desc'] = $request->blog_short_desc;
        $data['blog_long_desc'] = $request->blog_long_desc;

        $data['publication_status'] = $request->publication_status;

        //for image Upload
        $image = $request->file('blog_image');

        if ($image) {
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'blog_image/';
            $image_url = $upload_path . $image_full_name;
            $success = $image->move($upload_path, $image_full_name);
            if ($success) {
                $data['blog_image'] = $image_url;
                DB::table('tbl_blog')
                        ->where('blog_id', $blog_id)
                        ->update($data);
//                Session::put('message', 'Update blog information Successfully');
                return redirect::to('/manage-blog');
            }
        } else {
            DB::table('tbl_blog')
                    ->where('blog_id', $blog_id)
                    ->update($data);
//            Session::put('message', 'Update blog information Successfully');
            return redirect::to('/manage-blog');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
