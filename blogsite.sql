-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 28, 2016 at 01:54 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blogsite`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_12_26_051808_create_tbl_admin_table', 1),
(6, '2016_12_26_130552_create_tbl_category_table', 2),
(7, '2016_12_26_153846_create_tbl_blog_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` int(10) UNSIGNED NOT NULL,
  `admin_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `admin_email_address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `admin_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_name`, `admin_email_address`, `admin_password`, `created_at`, `updated_at`) VALUES
(1, 'Ronnie', 'r@gmail.com', '202cb962ac59075b964b07152d234b70', NULL, NULL),
(2, 'Ronnie Areas', 'r@gmail.com', '698d51a19d8a121ce581499d7b701668', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blog`
--

CREATE TABLE `tbl_blog` (
  `blog_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `blog_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `blog_short_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `blog_long_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `blog_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_blog`
--

INSERT INTO `tbl_blog` (`blog_id`, `category_id`, `blog_title`, `blog_short_desc`, `blog_long_desc`, `publication_status`, `blog_image`, `created_at`, `updated_at`) VALUES
(5, 5, 'পপ', '                                                                                    <span style="color: rgb(68, 68, 68); font-family: SolaimanLipi; font-size: 16px; text-align: justify; background-color: rgb(255, 255, 255);">‘কেয়ারলেস হুইস্পার’</span>                                                ', '                                                                                    <p style="box-sizing: border-box; margin: 0px 0px 10px; color: rgb(68, 68, 68); font-family: SolaimanLipi; font-size: 16px; text-align: justify; background-color: rgb(255, 255, 255);">‘কেয়ারলেস হুইস্পার’খ্যাত ব্রিটিশ পপ সুপারস্টার জর্জ মাইকেল আর নেই। ৫৩ বছর বয়সে ইংল্যান্ডের অক্সফোর্ডশায়ারের নিজ বাসায় মৃত্যুবরণ করেন এই কিংবদন্তি শিল্পী।</p><p style="box-sizing: border-box; margin: 0px 0px 10px; color: rgb(68, 68, 68); font-family: SolaimanLipi; font-size: 16px; text-align: justify; background-color: rgb(255, 255, 255);"><br></p>                                                                        ', 1, 'blog_image/NgBNeRTwl1TGJ59mXB8n.jpg', NULL, NULL),
(6, 6, 'নতুন বছরের প্রথম দিনে ৩৬ কোটি ২২ লক্ষ নতুন বই বিতরণ', '<span style="color: rgb(68, 68, 68); font-family: arial, helvetica, sans-serif; font-size: 14px; text-align: justify; background-color: rgb(255, 255, 255);">নতুন&nbsp;বছরের প্রথম দিনে অর্থাৎ ১&nbsp;জানুয়ারি সারাদেশে একযোগে প্রাথমিক ও মাধ্যমিক স্তরের শিক্ষার্থীদের বিনামূল্যে ৩৬ কোটি ২১ লক্ষ ৮২ হাজার ২৪৫টি নতুন বই প্রদান করা হবে বলে জানিয়েছেন শিক্ষামন্ত্রী নুরুল ইসলাম নাহিদ। ২০১৬ সালের তুলানয় ২০১৭ সালে ২ কোটি ৮৪ লক্ষ ১৯ হাজার ৪৮৫টি বই বেশি দেওয়া হবে।</span>', '<p style="box-sizing: border-box; margin: 0px 0px 10px; color: rgb(68, 68, 68); font-family: SolaimanLipi; font-size: 16px; text-align: justify; background-color: rgb(255, 255, 255);"><span style="box-sizing: border-box; font-size: 14px;"><span style="box-sizing: border-box; font-family: arial, helvetica, sans-serif;">নতুন&nbsp;বছরের প্রথম দিনে অর্থাৎ ১&nbsp;জানুয়ারি সারাদেশে একযোগে প্রাথমিক ও মাধ্যমিক স্তরের শিক্ষার্থীদের বিনামূল্যে ৩৬ কোটি ২১ লক্ষ ৮২ হাজার ২৪৫টি নতুন বই প্রদান করা হবে বলে জানিয়েছেন শিক্ষামন্ত্রী নুরুল ইসলাম নাহিদ। ২০১৬ সালের তুলানয় ২০১৭ সালে ২ কোটি ৮৪ লক্ষ ১৯ হাজার ৪৮৫টি বই বেশি দেওয়া হবে।</span></span></p><p style="box-sizing: border-box; margin: 0px 0px 10px; color: rgb(68, 68, 68); font-family: SolaimanLipi; font-size: 16px; text-align: justify; background-color: rgb(255, 255, 255);"><span style="box-sizing: border-box; font-size: 14px;"><span style="box-sizing: border-box; font-family: arial, helvetica, sans-serif;">২০১৬ সালের ১ জানুয়ারি ৩৩ কোটি ৩৭ লক্ষ ৬২ হাজার ৭৬০টি বই শিক্ষার্থীদের বিনামূল্যে দেওয়া হয়েছিল। প্রতি বছরেই বই বিতরণের সংখ্যা বাড়ছে। প্রি-প্রাইমারি, প্রাইমারি থেকে মাধ্যমিক পর্যন্ত স্কুল মাদ্রাসার ছাত্র-ছাত্রীদের এ বই দেওয়া হবে বলে জানান মন্ত্রী।</span></span></p><p style="box-sizing: border-box; margin: 0px 0px 10px; color: rgb(68, 68, 68); font-family: SolaimanLipi; font-size: 16px; text-align: justify; background-color: rgb(255, 255, 255);"><span style="box-sizing: border-box; font-size: 14px;"><span style="box-sizing: border-box; font-family: arial, helvetica, sans-serif;">মঙ্গলবার রাজধানীল কৃষিবিদ ইনস্টিটিউশন বাংলাদেশ মিলনায়তনে শিক্ষার উন্নত পরিবেশ, জঙ্গিবাদমুক্ত শিক্ষাঙ্গন শীর্ষক শিক্ষক সমাবেশে প্রধান অতিথির বক্তব্যে তিনি এসব তথ্য জানান। মাধ্যমিক ও উচ্চ মাধ্যমিক শিক্ষা বোর্ড ঢাকা এ সমাবেশের আয়োজন করে। শিক্ষামন্ত্রী বলেন, জঙ্গি তৎপরতায় যুক্ত হয়ে নিজেকে ধ্বংস ও সমাজ সভ্যতার সর্বনাশ ছাড়া কিছু করা যায় না। ইসলাম শান্তির ধর্ম। মানবতার ধর্ম। কল্যাণের ধর্ম। জঙ্গি তৎপরতায় যুক্ত হয়ে শুধু নিজেকে নয় মহান এই ধর্মকেও বিতর্কিত করা হয়েছে। স্নেহ, মমতা ও ভালবাসা দিয়ে শিক্ষার্থিদের জঙ্গি তৎপরতায় যুক্ত হওয়ার হাতছানি থেকে দূরে রাখতে হবে।</span></span></p><p style="box-sizing: border-box; margin: 0px 0px 10px; color: rgb(68, 68, 68); font-family: SolaimanLipi; font-size: 16px; text-align: justify; background-color: rgb(255, 255, 255);"><span style="box-sizing: border-box; font-size: 14px;"><span style="box-sizing: border-box; font-family: arial, helvetica, sans-serif;">শিক্ষকদের উদ্দেশ্যে তিনি বলেন, আমাদের নতুন প্রজন্ম মেধার দিক থেকে দরিদ্র নয়। তারা একদিন বিশ্ব জয় করবে। শুধু মানসম্মত শিক্ষা দিলেই হবেনা। আমরা সততা, ন্যায়নিষ্ঠ, শ্রদ্ধাশীল ও পরিপূর্ণ ভাল মানুষ চাই। সমাবেশে বিশেষ অতিথির বক্তব্য দেন শিক্ষা মন্ত্রণালয়ের সচিব সোহরাব হোসেন। সভাপতিত্ব করেন মাধ্যমিক ও উচ্চ মাধ্যমিক শিক্ষা বোর্ড ঢাকার চেয়ারম্যান প্রফেসর ড. ওয়াহেদুজ্জামান।</span></span></p>', 1, 'blog_image/WgNtVZyLulOYi8yUltv1.jpg', NULL, NULL),
(7, 4, 'ট্রাম্প টাওয়ারে বোমাতঙ্ক', '<span style="color: rgb(68, 68, 68); font-family: arial, helvetica, sans-serif; font-size: 14px; text-align: justify; background-color: rgb(255, 255, 255);">আমেরিকার নবনির্বাচিত প্রেসিডেন্ট ডোনাল্ড ট্রাম্পের টাওয়ারে মঙ্গলবার বিকেলে একটি মালিকানাবিহীন ব্যাগ শনাক্ত করা হয়। ব্যাগের ভেতরে বোমা বা বিস্ফোরক কিছু থাকতে পারে এ সন্দেহে হইচই পড়ে যায়।</span>', '<div><span style="color: rgb(68, 68, 68); font-family: arial, helvetica, sans-serif; font-size: 14px; text-align: justify; background-color: rgb(255, 255, 255);">আমেরিকার নবনির্বাচিত প্রেসিডেন্ট ডোনাল্ড ট্রাম্পের টাওয়ারে মঙ্গলবার বিকেলে একটি মালিকানাবিহীন ব্যাগ শনাক্ত করা হয়। ব্যাগের ভেতরে বোমা বা বিস্ফোরক কিছু থাকতে পারে এ সন্দেহে হইচই পড়ে যায়।</span></div><div><br></div><div><p style="box-sizing: border-box; margin: 0px 0px 10px; color: rgb(68, 68, 68); font-family: SolaimanLipi; font-size: 16px; text-align: justify; background-color: rgb(255, 255, 255);"><span style="box-sizing: border-box; font-size: 14px;"><span style="box-sizing: border-box; font-family: arial, helvetica, sans-serif;">নিউইয়র্ক শহরে অবস্থিত টাম্প টাওয়ারে সবসময় ভিড় থাকে। কারণ, ৫৮ তলার আকাশছোঁয়া ওই টাওয়ারে অনেক অফিস, ব্যবসায়িক ক্ষেত্র এবং আবাসনও রয়েছে। বোমাতঙ্ক ছড়ালে প্রাণ বাঁচাতে সকলেই এদিক ওদিক পালাতে থাকেন। এতে মারাত্মক বিশৃঙ্খলার সৃষ্টি হয়। স্থানীয় পুলিশের পাশাপাশি ডাকা হয় বম্ব স্কোয়াডকে। কিন্তু বিপজ্জনক কিছু পাওয়া যায়নি। বাচ্চাদের কিছু খেলনা ছিল ওই ব্যাগে।</span></span></p><p style="box-sizing: border-box; margin: 0px 0px 10px; color: rgb(68, 68, 68); font-family: SolaimanLipi; font-size: 16px; text-align: justify; background-color: rgb(255, 255, 255);"><span style="box-sizing: border-box; font-size: 14px;"><span style="box-sizing: border-box; font-family: arial, helvetica, sans-serif;">দীর্ঘ চার বছর পর নির্মাণকাজ শেষ হওয়ার পর ট্রাম্প টাওয়ারের উদ্বোধন হয় ১৯৮৩ সালের ৩০ নভেম্বর। মঙ্গলবার বিকেলে এ বোমাতঙ্কের মধ্যে পড়তে হয়নি ট্রাম্পকে। ওই সময় তিনি ছিলেন ফ্লোরিডায়।</span></span></p></div><div><span style="box-sizing: border-box; font-size: 14px;"><span style="box-sizing: border-box; font-family: arial, helvetica, sans-serif;"><br></span></span></div>', 1, 'blog_image/irThJFomyR2OMxE4GBwQ.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `category_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `publication_status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`category_id`, `category_name`, `category_desc`, `publication_status`, `created_at`, `updated_at`) VALUES
(3, 'বিজ্ঞান ও প্রযুক্তি', '                                                                                                                                                                                                ----------                                                        ', 1, NULL, NULL),
(4, 'বিনোদন', '----------------', 1, NULL, NULL),
(5, 'নগর-মহানগর', '--------------', 1, NULL, NULL),
(6, 'শিক্ষাঙ্গন', '---------------', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_blog`
--
ALTER TABLE `tbl_blog`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_blog`
--
ALTER TABLE `tbl_blog`
  MODIFY `blog_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
