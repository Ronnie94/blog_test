@extends('master')
@section('content')

<div class="post_content">
    <h2>{{$blog_info_details->blog_title}}</h2>
    <strong>Author:</strong> Templatemo  || <strong>Category:</strong> <a href="#">{{$blog_info_details->category_name}}</a>
   <img src="{{URL::to($blog_info_details->blog_image)}}" width="480" height="300"/>
   <p>{!!$blog_info_details->blog_long_desc !!}</p>

</div>

@stop
