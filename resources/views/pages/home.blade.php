@extends('master')

@section('content')

<div class="post_section">

    <div class="post_date">
        24<span>Oct</span>
    </div>
    @foreach($published_blog as $single)
    <div class="post_content">
        <h2><a href="blog_post.html">{{$single->blog_title}}</a></h2>
        <strong>Author:</strong> Templatemo <strong>Category:</strong> <a href="#">{{$single->category_name}}</a>
        <img src="{{$single->blog_image}}" width="480" height="300"/>
        <p>{!!$single->blog_short_desc !!}</p>
        <a href="blog_post.html">58 views</a> | <a href="{{URL::to('/blog-details/'.$single->blog_id)}}">Continue reading...</a>

    </div>
    @endforeach

    <div class="cleaner"></div>
</div>
@stop