@extends('master')
@section('recent')
<h4>Recent Post</h4>
<?php
$recent_blog = DB::table('tbl_blog')
        ->orderBy('blog_id', 'desc')
        ->where('publication_status',1)
        ->take(2)
        ->get();
?>
<ul class="templatemo_list">
    @foreach($recent_blog as $list_blog)
    <li><a href="#" target="_parent">{{$list_blog->blog_title}}</a></li>
    @endforeach
</ul>
@stop
