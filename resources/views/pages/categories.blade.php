@extends('master')

@section('categories')

<h4>Categories</h4>
<?php
    $category_list = DB::table('tbl_category')
            ->where('publication_status',1)
            ->get();
?>
<ul class="templatemo_list">
    @foreach($category_list as $cat_list)
    <li><a href="#">{{$cat_list->category_name}}</a></li>
    @endforeach
</ul>
@stop
