@extends('admin.admin_master')
@section('admin_content')
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>
        <a href="index.html">Home</a>
        <i class="icon-angle-right"></i> 
    </li>
    <li>
        <i class="icon-edit"></i>
        <a href="#">Forms</a>
    </li>
</ul>

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Form Elements</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>


        <h3 style="color: green;">

        </h3>
        <div class="box-content">

            {!! Form::open(['url' => '/update-blog', 'method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="typeahead">Blog Title</label>
                    <div class="controls">
                        <input type="text" name="blog_title" value="<?php echo $blog_info->blog_title; ?>" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4"/> 
                    </div>
                </div>
                <input type="hidden" name="blog_id" value="<?php echo $blog_info->blog_id; ?>" /> 

                <div class="control-group">
                    <label class="control-label" for="typeahead"> Select Category </label>
                    <div class="controls">
                        <select name="category_id">
                            <!--<option value="<?php // echo $blog_info->category_id; ?>"><?php // echo $blog_info->category_name; ?></option>-->
                            @foreach($cat_list as $s_cat)
                            <option value="{{$s_cat->category_id}}"> {{$s_cat->category_name}} </option>
                            @endforeach
                        </select>
                    </div>
                </div>


                <div class="control-group hidden-phone">
                    <label class="control-label" for="textarea2">Blog Short Description</label>
                    <div class="controls">
                        <textarea class="cleditor" name="blog_short_desc" id="textarea2" rows="3">
                            <?php echo $blog_info->blog_short_desc; ?>
                        </textarea>
                    </div>
                </div>


                <div class="control-group hidden-phone">
                    <label class="control-label" for="textarea2">Blog long Description</label>
                    <div class="controls">
                        <textarea class="cleditor" name="blog_long_desc" id="textarea2" rows="3">
                            <?php echo $blog_info->blog_long_desc; ?>
                        </textarea>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="typeahead"> Publication Status </label>
                    <div class="controls">
                        <select name="publication_status">
                            <option value="<?php echo $blog_info->publication_status; ?>">Select Status</option>
                            <option value="1">Published</option>
                            <option value="0">Unpublished</option>
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="fileInput">File input</label>
                    <div class="controls">
                        <input class="input-file uniform_on" id="fileInput"  type="file" name="blog_image">
                    </div>
                </div>  

                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                    <button type="reset" class="btn">Cancel</button>
                </div>
            </fieldset>
            {!! Form::close() !!}

        </div>
    </div><!--/span-->

</div><!--/row-->

@stop
